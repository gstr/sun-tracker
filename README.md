# sun tracker

Sun Tracker

En el presente repositorio se encontraran los archivos correspondientes a un seguidor solar inteligente. 
El mismo fue implementado por los alumnos de 5to año de la carrera Ingeniería en Telecomunicaciones de la Universidad Nacional de Río Cuarto.


En el contexto de la materia Aplicaciones TCP-IP se presentó como trabajo final, contemplando el seguidor solar y un pagina web para visualizar los datos. Lo importante de las visualizaciones es que permiten llevar un monitoreo en tiempo real de las variables indispensables para el funcionamiento del sistema , como "GradosX" ,"GradosY" y "ValCeldas". Dichas variables corresponden a los grados de los servomotores que controlan la posición y el valor de voltaje que miden las celdas.

1- El seguidor solar fue montado sobre una Arduino UNO.

2- La adquisición y preparación de los datos para la página web fue realizada con Python 3.6.7

Todo lo relacionado a 1 y 2 estará presente en la carpeta "Seguidor Solar y preparación de datos"

3- El servidor web se montó sobre una Raspberry Pi 2 , utilizando servidores Apache2 que sirven contenido html y php momentáneamente.

Todo lo relacionado a 3 se encuentra en la carpeta "Servidor Web".

En definitiva, lo que se necesita para montar este escenario es :

-> 1 Arduino UNO

->2 ServoMotores

->4 Sensores LDR

->1 Raspberry Pi 2

->Celdas Solares

->Filtro paso bajo para la entrada analogica

Por último queremos hacer mención que si bien todo este desarrollo fue presentado para una asignatura 
,es materia de investigación para el grupo "GSTR"- Grupo de Sistemas de Tiempo Real,
el cual se encuentra actualmente investigando sobre una estación meteorológica.
Nuestro aporte es lograr que dicha estación logre ser autónoma del punto de vista energético.
