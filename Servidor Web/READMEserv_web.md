Este directorio contiene todos los archivos minimos necesarios para la puesta en marcha de la pagina web. La misma cuenta con 4 pestañas denominadas: home, paneles, clima y desarrolladores.
El contenido de las pestañas es:
    Home: Solicitud de ingreso mediante usuario previamente cargado por el administrador en el archivo "usuarios.txt"
    Paneles: Visualización de los últimos datos de las celdas solares, junto con su ubicación en grados (horizontal y vertical). Además, gráficas representativas de los distintos valores de ubicación adoptados y la carga de las celdas.
    Clima: Estado del clima en el lugar geográfico que se encuentra el seguidor solar, dato relevante ya que tendra total relacion con la carga que ofreceran las celdas.
    Desarrolladores: pestaña destinada exclusivamente para dar información sobre los estudiantes intervinientes en este proyecto, junto con el contacto de cada uno de ellos.
    
Para la correcta puesta en marcha del servidor se debe tener previamente instalado el servidor Apache2. Asumiendo que este paso ya esta realizado, se debe dirigir al directorio /etc/apache2/sites-available y pegar el archivo "seguidorsolar.conf", luego en consola '#a2ensite seguidorsolar.conf'. Por último se crea el directorio '/var/www/seguidorsolar' el cual contendra todos los archivos .html, .php, .txt, etc.

