#include <Servo.h> // libreria para los servo motores 
//----------------------------------------------------------------------------------------------------
//Configuracion Inicial:
int Resultados[]={90,125,0,0,0,0,0,0,0,0};//Gradosx , Gradosy , promedioSalidaLDRSup , promedioSalidaLDRSup , promedioSalidaLDRIzquierda , promedioSalidaLDRDerecha ,Diferencia Vertical , Diferencia Horizontal , CeldaA4 , CeldaA5... inicializamos los valores en 0

// 180 horizontal MAXIMO
//Resultados[0]=servoHorizontal
//Resultado[1]=servoVertical
int EsInicServoH=Resultados[0];
int EsInicServoV=Resultados[1];
//----------------------------------------------------------
// declaramos el valor para el servo horizontal  
Servo horizontal;      

int servohLimitHigh = 150; //limite en posicion superior
int servohLimitLow = 30; //limite para posicion inferior
//----------------------------------------------------------
// declaramos los valores para el servo vertical   
Servo vertical;      

int servovLimitHigh = 160; //limite en posicion superior
int servovLimitLow = 20; //limite para posicion inferior


//----------------------------------------------------------
//conexion a las resistencias LDR
int ldrlt = A0;  
int ldrrt = A1; 
int ldrld = A2; 
int ldrrd = A3; 

int lt; //(superior izquierdo)
int rt; //(superior derecho)
int ld; //(inferior izquierdo)
int rd; //(inferior derecho)


int avt; // valor superior maximo
int avd;// valor inferior maximo
int avl;// valor izquierdo maximo
int avr;// valor derecho maximo

int dvert;// diferencia entre superior e inferior
int dhoriz; // diferencia entre izquierdo y derecho

//----------------------------------------------------------
// Tolerancia de los LDR:
int tol = 15; 
//----------------------------------------------------------
//Configuracion de celdas solares:
int PinCeldaA4=A4;
int PinCeldaA5=A5;
int valoresCeldas[][6] = {{0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}};
int valoresCeldasTraq[][3] ={{0,0,0},{0,0,0},{0,0,0}};

//-----------------------------------------------------------
//Variables para control:
int MovimientoCorreccion_intentos = 0;
int buzzer = 7; 
int count=0;
int corte;
int ValorNuevoX;
int ValorNuevoY;
int umbralsolar = 0;
const int mapX[]={30,50,70,90,110,130,150};
const int mapY[]={30,50,70,90,110,130,150};
const int MinX[]={10,0,-10};
const int MinY[]={10,0,-10};
int Controlumbral;
//-----------------------------------------------------------------------------------

//Iniciamos la configuracion de Setup
void setup() {
  
  Serial.begin(115200);
  
  horizontal.attach(5); //servo horizontal pin digital 9
  vertical.attach(6); // servo vertical pin digital 10
  pinMode(buzzer,OUTPUT);
  CorrimientoGradosHorizontal(horizontal.read(),90);
  
  CorrimientoGradosVertical(vertical.read(),90);

  corte=0;
  Controlumbral=0;
  ubicacion();
  Mediciones();

  
}


//Arrancamos el programa principal en el loop

void loop() { 

  if (corte == 0)
  {
  
  while (((-1*tol < Resultados[6] && Resultados[6] < tol) && ((-1*tol < Resultados[7] && Resultados[7] < tol))) != true ){
  Movimiento(Resultados[0],Resultados[1],tol,servovLimitHigh,servovLimitLow,servohLimitHigh,servohLimitLow);
  //Mostrando Valores de Posiciones X e Y de los motores:
    Serial.print("Datos:");
    Serial.print(" ");
    Serial.print(Resultados[0]);
    Serial.print(" ");
    Serial.print(Resultados[1]);
    Serial.print(" ");
    Serial.print(Resultados[8]);
    Serial.print(" ");
    Serial.println();
  }
  Serial.println(".....Estoy Orientado......PrimeraParteTerminada");
  delay(1000);
  Resultados[8]=analogRead(PinCeldaA4);
  Resultados[9]=analogRead(PinCeldaA5);
  
  if ((Resultados[8] >= umbralsolar) && (Resultados[8] >= umbralsolar) && (count <= 5)){
    
    
    while((Resultados[8] >= umbralsolar) && (Resultados[8] >= umbralsolar)){
      Serial.println("Estoy Traqueando El sol...ParteDosBien");

      
      Serial.print("Informacion:");
      Serial.print(" ");
      Serial.print(Resultados[0]);
      Serial.print(" ");
      Serial.print(Resultados[1]);
      Serial.print(" ");
      Serial.print(Resultados[8]);
      Serial.print(" ");
      Serial.println();
      
      delay(15000);

      
      while(MovimientoCorreccion_intentos < 3){
      MovimientoCorreccion_intentos = MovimientoCorreccion_intentos + 1;
      MovimientoCorreccion();  
      }
      MovimientoCorreccion_intentos = 0;
      Resultados[8]=analogRead(PinCeldaA4);
      Resultados[9]=analogRead(PinCeldaA5);
      
    }
    count=0;
    
  }
  else if ((Resultados[8] < umbralsolar) && (Resultados[8] < umbralsolar) && (count < 5)){

    Serial.println("Me Oriente pero no Tengo Luz solar ... ParteDosMal");
    
    ubicacion();    
    Mediciones();
    count=count+1;
  }
  else {
    corte=1;
    Serial.println(".......Volviendo A Grados Originales Grados X:........");
    CorrimientoGradosHorizontal(Resultados[0],EsInicServoH);
    Serial.println(".......Volviendo A Grados Originales Y:........");
    CorrimientoGradosVertical(Resultados[1],EsInicServoV);
    Serial.println("NO HAY POSIBILIDAD DE RECUPERAR LA SITUACION ... LLAMAR MANTENIMIENTO!");
    delay(60000);
  }
  }
  else
  {
    //Ampliar analisis para hacer que todos los dias pueda traquear
    horizontal.detach();
    vertical.detach();
    Serial.println("ERROR!");
    delay(10000);
  }
}


/*FUNCIONES*/

void Movimiento(int x , int y, int z, int LimitvHigh , int LimitvLow, int LimithHigh, int LimithLow){// "x" es servo horizontal , "y" es servo vertical , "z" es la tolerancia , "LimitvHigh" Limite Vertical superior
//Definimos todas las salidas , valga la redundancia en los nombres 

Mediciones();


if (-1*z > Resultados[6] || Resultados[6] > z) // revisar la diferencia para cambiar al angulo vertical
 {
  
  vertical.attach(6);
  delay(30);
  if (avt > avd)
    {
//      
      y = ++y; //Suma al Vertical
     if (y > LimitvHigh) 
     { 
      y = LimitvHigh; //Fija el valor del Vertical al maximo
   
     }
    }
  else if (avt < avd)
    {
    y= --y; //resta al Vertical
    if (y < LimitvLow)
  {
    y = LimitvLow; //Fija el valor del Vertical al minimo
  }
  }
  vertical.write(y);
  vertical.detach(); 
  
    
 }
   
if (-1*z > Resultados[7] || Resultados[7] > z) // revisar la diferencia para cambiar al angulo horizontal
   {
    
    horizontal.attach(5);
    delay(23);
    if (avl > avr)
    {
    x = --x; //resta al horizontal
    
    if (x < LimithLow)
    {
    x = LimithLow; //Fija el valor del horizontal al minimo 
    }
    }
    else if (avl < avr)
    {
    x = ++x; //Suma al horizontal
     if (x > LimithHigh)
     {
     x = LimithHigh; //Fija el valor del horizontal al maximo
     }
     }
  else if (avl = avr)
    {
    
    }
  
  horizontal.write(x);
  horizontal.detach();
   }
  Resultados[0]=x;
  Resultados[1]=y;
  Resultados[2]=avt;
  Resultados[3]=avd;
  Resultados[4]=avl;
  Resultados[5]=avr;
  
  
   
   
}

//Funcion de cambio de angulos Lento : 

void CorrimientoGradosVertical(int Xv, int Xn)
{
  
  vertical.attach(6);
  while(Xv != Xn)
  {
    if (Xv < Xn)
    {
      Xv=Xv+1;
      vertical.write(Xv);
      Serial.println(Xv);
      delay(50);
    }
    else 
    {
      Xv=Xv-1;
      vertical.write(Xv);
      Serial.println(Xv);
      delay(50);
    }
  }
  vertical.detach();
  Resultados[1]=Xn;
}

void CorrimientoGradosHorizontal(int Xv, int Xn)
{
  
  horizontal.attach(5);
  while(Xv != Xn)
  {
    if (Xv < Xn)
    {
      Xv=Xv+1;
      horizontal.write(Xv);
      Serial.println(Xv);
      delay(50);
    }
    else 
    {
      Xv=Xv-1;
      horizontal.write(Xv);
      Serial.println(Xv);
      delay(50);
    }
  }
  horizontal.detach();
  Resultados[0]=Xn;
}

void Mediciones (){
   lt = analogRead(ldrlt); 
   rt = analogRead(ldrrt); 
   ld = analogRead(ldrld); 
   rd = analogRead(ldrrd); 


   avt = (lt + rt) / 2; 
   avd = (ld + rd) / 2; 
   avl = (lt + ld) / 2; 
   avr = (rt + rd) / 2; 

   dvert = avt - avd; 
   dhoriz = avl - avr;
   
   Resultados[6]=dvert;
   Resultados[7]=dhoriz;
}

void ubicacion(){
  
int maxValor = 0 ; //Calculando valor maximo de celdas
byte maxfila=0;
byte maxCol=0;
/*cargando valores de matriz celdas*/
    for (int fila = 0; fila < 7; fila++)
  {
    CorrimientoGradosHorizontal(horizontal.read(),mapX[fila]);
    for (int col = 0; col < 7; col++)
    {
     CorrimientoGradosVertical(vertical.read(),mapY[col]);
     valoresCeldas[fila][col]=analogRead(PinCeldaA4);
        
    }
  }

/*Para encontrar el maximo*/    
    for (int fila = 0; fila < 6; fila++)
  {
    for (int col = 0; col < 6; col++)
    {
      Serial.print(valoresCeldas[fila][col]);
      Serial.print("\t");
      if (valoresCeldas[fila][col] > maxValor)
      {
        maxValor = valoresCeldas[fila][col];
        maxfila = fila;
        maxCol = col;
      }
    }
    Serial.println();
  }
  Serial.println();
  Serial.print("Maximo valor es : ");
  Serial.print(maxValor);
  Serial.print(" ubicado en fila ");
  Serial.print(maxfila);
  Serial.print(" columna ");
  Serial.println(maxCol);

  umbralsolar = valoresCeldas[maxfila][maxCol];
  Controlumbral=umbralsolar - 145; //Verificamos que el umbral sea mayor 0.8[volts] 
  if (Controlumbral <= 0){
    corte = 1;
  }
  else{
/*Desplazamiento para trackear*/
    umbralsolar=umbralsolar-62; //le permitimos a los valores de celda una variacion de 0.3 volt
    tone(buzzer, 700);
    delay(500);
    tone(buzzer, 900);
    delay(500);
    noTone(buzzer);
    CorrimientoGradosHorizontal(horizontal.read(),mapX[maxfila]);
    CorrimientoGradosVertical(vertical.read(),mapY[maxCol]);
    Resultados[0]=mapX[maxfila];
    Resultados[1]=mapY[maxCol];
    tone(buzzer, 900);
    delay(500);
    tone(buzzer, 1100);
    delay(500);
    noTone(buzzer);
    
  }
  
}

void MovimientoCorreccion()
{
  int maxValor = 0 ; //Calculando valor maximo de celdas
  byte maxfila=0;
  byte maxCol=0;
  int gradosRelativos = 0;
/*cargando valores de matriz celdas*/
    for (int fila = 0; fila < 3; fila++)
  {
    gradosRelativos = Resultados[0] + MinX[fila];
    delay(100);
    Serial.print("El movimiento relativo de X debe ser: ");
    if (gradosRelativos < 30 ){
        gradosRelativos = 30;
        }
    else if (gradosRelativos > 150 ){
        gradosRelativos = 150;
        }
    Serial.println(gradosRelativos);
    CorrimientoGradosHorizontal(horizontal.read(),gradosRelativos);
    delay(200);
    for (int col = 0; col < 3; col++)
    {
      gradosRelativos = Resultados[1] + MinY[col];
      if (gradosRelativos < 20){
        gradosRelativos = 20;
        }
      else if (gradosRelativos > 150 ){
        gradosRelativos = 150;
        }
      Serial.print("El movimiento relativo de Y debe ser: ");
      Serial.println(gradosRelativos);
     CorrimientoGradosVertical(vertical.read(),gradosRelativos);
     delay(200);
     valoresCeldasTraq[fila][col]=analogRead(PinCeldaA4);
        
    }
    delay(200);
    CorrimientoGradosVertical(vertical.read(),Resultados[1]);
    delay(200);
    CorrimientoGradosHorizontal(horizontal.read(),Resultados[0]);
  }
  /*Para encontrar el maximo*/    
    for (int fila = 0; fila < 3; fila++)
    {
    for (int col = 0; col < 3; col++)
    {
      Serial.print(valoresCeldasTraq[fila][col]);
      Serial.print("\t");
      if (valoresCeldasTraq[fila][col] > maxValor)
      {
        maxValor = valoresCeldasTraq[fila][col];
        maxfila = fila;
        maxCol = col;
      }
    }
    Serial.println();
    }
    Serial.println();
    Serial.print("Maximo valor es : ");
    Serial.print(maxValor);
    Serial.print(" ubicado en fila ");
    Serial.print(maxfila);
    Serial.print(" columna ");
    Serial.println(maxCol);
    
    umbralsolar = valoresCeldasTraq[maxfila][maxCol] ;
    Controlumbral=umbralsolar - 512; // Verificamos que el umbral encontrado en el sol sea mayor a 2.5 volt
    if (Controlumbral>=0){
    MovimientoCorreccion_intentos = 3;  
    umbralsolar = umbralsolar - 103; // Le permitimos a estos valores de umbral oscilar a partir de un valor minimo de 2 volt
    
    if ((Resultados[0]+ MinX[maxfila] > 30) && (Resultados[0] + MinX[maxfila]) < 150){
          CorrimientoGradosHorizontal(horizontal.read(),Resultados[0]+ MinX[maxfila]);
          Resultados[0]=Resultados[0] + MinX[maxfila];
    }
    else if (Resultados[0]+ MinX[maxfila]<30)
    {
          CorrimientoGradosHorizontal(horizontal.read(),30);
          Resultados[0]=30;
    }
    else if (Resultados[0]+ MinX[maxfila] >150){
          CorrimientoGradosHorizontal(horizontal.read(),150);
          Resultados[0]=150;
    }
    if ((Resultados[1]+ MinY[maxCol] > 20) && (Resultados[1] + MinY[maxCol]) < 150)
    {
          CorrimientoGradosVertical(vertical.read(), Resultados[1]+ MinY[maxCol]);
          Resultados[1]=Resultados[1] + MinY[maxCol];
    }
    else if (Resultados[1]+ MinY[maxCol]<20)
    {
        CorrimientoGradosVertical(vertical.read(), 20);
        Resultados[1]=20;
    }
    else if (Resultados[1]+ MinY[maxCol]>150)
    {
        CorrimientoGradosVertical(vertical.read(), 150);
        Resultados[1]=150;
      
    }
    
    Serial.print("Los valores nuevos de Resultados[0]: ");
    Serial.println(Resultados[0]);
    Serial.print("Los valores nuevos de Resultados[1]: ");
    Serial.println(Resultados[1]);
    Mediciones();
    while (((-1*tol < Resultados[6] && Resultados[6] < tol) && ((-1*tol < Resultados[7] && Resultados[7] < tol))) != true ){
      Movimiento(Resultados[0],Resultados[1],tol,servovLimitHigh,servovLimitLow,servohLimitHigh,servohLimitLow);
      }
    
   }
   
   else if (MovimientoCorreccion_intentos == 3) {
    umbralsolar = 1050; // condicion tal que no hay forma que los valores cuantizados entren en este valor , por lo tanto sale ...
   }
}



