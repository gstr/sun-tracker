#!/usr/bin/python3

import serial
import time
from matplotlib import pyplot as plt
from matplotlib import style
import numpy as np
import shutil, os
from PIL import Image
AnchoIma=10
AltoIma=9
#-----------------------------------------------
count1min=0
count1dia=0
arduino=serial.Serial('/dev/ttyUSB0',115200)
arduino.flushInput()
contenidoGraficoX=[]
contenidoGraficoY1=[]
contenidoGraficoY2=[]
contenidoGraficoY0=[]
#------------------------------------------------
rutaOrig = os.getcwd() + os.sep
rutaDest = '/var/www/seguidorsolar/'
#-------------------------------------------------
rutaArch='/var/www/seguidorsolar/'

#-------------------------------------------------

with arduino:

    while True:
        try:
            #---------------------------------------------------------
            #Preparacion de datos recibidos
            puertoescucha=arduino.readline()
            dato=puertoescucha.decode('ascii',errors='replace')
            Trdato=dato.split(" ")
            print(Trdato)
            if(Trdato[0]=="Datos:"):
                print(dato)
            elif(Trdato[0]=="Informacion:"):
                dia=time.strftime("%d %m %Y")
                momento=time.strftime("%H:%M:%S")
                ValCelda=float(Trdato[3])*(5/1024)
                Elementos_Guardado=(Trdato[1],Trdato[2],str(ValCelda)+" V")
                Recibido=dia +"@@"+momento + "@@" + "@@".join(Elementos_Guardado) + "//"
            #---------------------------------------------------------
                #Testeando Variables:
                print(Trdato)
                print(" ")
                print(len(Trdato))
            #----------------------------------------------------------
                if (count1min<9):
            #-----------------------------------------------------
            #Guardando datos ordenados para Armar Graficos
                    ArchivoWr=open(rutaArch + 'datos.txt',"a")
                    contenidoGraficoX.append(momento)
                    contenidoGraficoY0.append(int(Trdato[1]))
                    contenidoGraficoY1.append(int(Trdato[2]))
                    contenidoGraficoY2.append(float(Trdato[3])*(5/1024))
            #---------------------------------------------------------
            #Testeando Variables:
                    print(contenidoGraficoX)
                    print(" ")
                    print(" ")
                    print("longitud X:" + str(len(contenidoGraficoX)))
                    print(" ")
                    print(" ")
                    print(" ")
                    print(contenidoGraficoY0)
                #-----------------------------------------------------
                    ArchivoWr.writelines(Recibido)
                    count1min=count1min+1
                    ArchivoWr.close()
                else:
                    count1dia=count1dia+1
                    count1min=0
                #-----------------------------------------------------
                    style.use('fivethirtyeight')
                    plt.figure(figsize=(AltoIma,AnchoIma)) 
                    plt.xlabel("hora")
                    plt.ylabel("Posicion Angular Vertical Servo")
                    plt.title(dia)
                    plt.plot(np.arange(len(contenidoGraficoX)),contenidoGraficoY0)
                    plt.xticks(np.arange(len(contenidoGraficoX)), contenidoGraficoX,size='small',rotation = 45)
                    plt.savefig('datos_Servo_Vert.png')
                    Image.open('datos_Servo_Vert.png').save('datos_Servo_Vert.jpg','JPEG')
                    origen = rutaOrig + 'datos_Servo_Vert.jpg'
                    destino = rutaDest + 'datos_Servo_Vert.jpg'
                    try:
                        shutil.copyfile(origen, destino)
                        print("Archivo copiado")
                    except:
                        print("Se ha producido un error")
                #-----------------------------------------------------
                    plt.figure(figsize=(AltoIma,AnchoIma)) 
                    plt.xlabel("hora")
                    plt.ylabel("Posicion Angular Horizontal Servo")
                    plt.title(dia)
                    plt.plot(np.arange(len(contenidoGraficoX)),contenidoGraficoY1)
                    plt.xticks(np.arange(len(contenidoGraficoX)), contenidoGraficoX,size='small',rotation = 45)
                    plt.savefig('datos_Servo_Horiz.png')
                    Image.open('datos_Servo_Horiz.png').save('datos_Servo_Horiz.jpg','JPEG')
                    origen = rutaOrig + 'datos_Servo_Horiz.jpg'
                    destino = rutaDest + 'datos_Servo_Horiz.jpg'
                    try:
                        shutil.copyfile(origen, destino)
                        print("Archivo copiado")
                    except:
                        print("Se ha producido un error")
                #-----------------------------------------------------
                    plt.figure(figsize=(AltoIma,AnchoIma)) 
                    plt.xlabel("hora")
                    plt.ylabel("Carga Celda")
                    plt.title(dia)
                    plt.plot(np.arange(len(contenidoGraficoX)),contenidoGraficoY2)
                    plt.xticks(np.arange(len(contenidoGraficoX)), contenidoGraficoX,size='small',rotation = 45)
                    plt.savefig('datos_Carga_Celda.png')
                    Image.open('datos_Carga_Celda.png').save('datos_Carga_Celda.jpg','JPEG')
                    origen = rutaOrig + 'datos_Carga_Celda.jpg'
                    destino = rutaDest + 'datos_Carga_Celda.jpg'
                    try:
                        shutil.copyfile(origen, destino)
                        print("Archivo copiado")
                    except:
                        print("Se ha producido un error")
                #----------------------------------------------------------
                    contenidoGraficoX=[]
                    contenidoGraficoY0=[]
                    contenidoGraficoY1=[]
                    contenidoGraficoY2=[]
                #------------------------------------------------------
                if(count1dia==144):
                    ArchivoRr=open(rutaArch + 'datos.txt',"r")
                    print(ArchivoRr.read())
                    ArchivoRr.close()
                    count1dia=0
        except KeyboardInterrupt:
            print("Saliendo")
            break
