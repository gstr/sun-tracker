Los archivos presentes son : 

-> SunTracker.ino ... contiene todo el codigo para lograr el seguimiento del sol.

->PythonDatosVisualizacion.py.. contiene todo el codigo para capturar los datos via puerto serie , filtrarlos, escribir un archivo de texto que sirve de base de datos y armar las graficas.

SunTracker.ino:

El funcionamiento general se basa en : 

1- Generar mediciones con las celdas en un rango de 120 grados tanto para el eje "x" como para el eje "y", con un paso de 20 grados.
se forma una matriz de 7x7 que contiene las mediciones.Este mapeo en el espacio tiene la finalidad de encontrar el mejor punto para posicionar los servos.
Todo este procedimiento lo realiza la funcion "ubicacion()".
El objetivo es que el sistema inicialmente logre orientarse a groso modo mediante un escaneo general.

2- Luego de realizar los primeros movimientos con "ubicacion()" , el sistema busca orientarse completamente mediante la utilizacion de la funcion "movimiento()".
Esta funcion se basa en la utilizacion de los LDR.Lo que hace es sensar con los 4 LDR y agruparlos de a 2 , de tal forma de armar 4 variables que representan : El promedio de los 2 LDR superiores, 
El promedio de los 2 LDR inferiores , El promedio de los 2 LDR Izquierdos , El promedio de los 2 LDR Derechos.
Con dichas variables y un nivel de tolerancia, que representa la diferencia maxima entre los promedios, se busca orientar grado a grado los paneles solares.
Simplemente contar que si el promedio de los 2 LDR derechos es mayor que el promedio de los 2 LDR izquierdos , el servo motor horizontal se mueve un grado para la derecha. Con esta logica , se plantean
los 4 casos posibles.

3-Luego de tener la posicion exacta del punto de luz, se miden las celdas solares.
Si el valor de las celdas es igual a un umbral seteado por la funcion "ubicacion()" o lo supera  , el sistema entra en un estado denominado Traqueo.
Este estado representa el seguimiento del sol.

4-En el estado de traqueo , cada 1 minuto se mapea alrededor de la posicion actual con la finalidad de no perder la maxima intensidad de luz y si se detecta otra posicion diferente con mayor intensidad se procede a reubicarse.
Todo esto lo realiza la funcion "MovimientoCorrecion()". La particularidad de esta funcion es que la matriz de mapeo alrededor de la posicion actual resulta ser de 3x3 , ya que las medidas se toman en un rango +-10 grados respecto
a la posicion actual.

5- en el caso de que en dicho mapeo el valor maximo medido por las celdas no supere los 2.5 voltios , se intenta 3 veces consecutivas para no perder inmediatamente el estado de traqueo.

6- Si el estado de traqueo se pierde como producto de que se superaron los 3 intentos , el sistema vuelve inmediatamente a ejecutar la funcion "ubicacion()" y tratar de buscar nuevamente a nivel general un punto luminoso.

7- si el punto luminoso encontrado con la funcion "ubicacion()" no supera los 0.8 voltios , vuelve a intentar ... el sistema se da como margen 5 intentos consecutivos para remediar la situacion . Si no es asi , el sistema interpreta
que no existe en su rango de movimiento ningun As luminoso que valga la pena.

8- En dicho caso , el sistema vuelve a sus posiciones iniciales y se mantiene en reposo , mandando por serial mensajes de error.

(Hasta aqui ha llegado el desarrollo , se pretende ampliar)


PythonDatosVisualizacion.py:

1 - se abre el serial en las condiciones acordadas por ambos dispositivos (Arduino y Raspberry Pi).

2 - La Raspberry Pi se encuentra todo el tiempo en modo escucha , buscando en todos los datos recibidos por serial , aquellos que comiencen con el 
rotulo "Datos:" y "Informacion:". 

3- Si el rotulo del paquete recibido es "Datos:" , solamente imprime por pantalla el dato.

4- Si el rotulo del paquete recibido es "Informacion:" , separa los datos correspondientes a GradosX, GradosY y el valor de las celdas solares que le envia la Arduino UNO.

5- Estos datos  los va almacenando en variables junto con el momento instante en el que lo recibio para poder armar una grafica en funcion del tiempo.

6- Por cada paquete rotulado "Informacion:" se escribe un archivo de texto con los datos recibidos y con un formato establecido.

7- Por cada 10 paquetes rotulados "Informacion: se arman las graficas correspondientes"

8- en un dia completo la base de datos se limpia y se vuelve a escribir.